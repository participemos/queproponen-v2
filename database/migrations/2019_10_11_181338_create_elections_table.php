<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('elections', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('year');
            $table->integer('show_position');
            $table->string('name');
            $table->bigInteger('place_id')->unsigned();
			$table->foreign('place_id')->references('id')->on('places')->onDelete('cascade');
            $table->timestamps();
        });

		Schema::table('ballots', function (Blueprint $table) {
			$table->bigInteger('election_id')->unsigned()->nullable()->default(null);
			$table->foreign('election_id')->references('id')->on('elections')->onDelete('cascade');
		});
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('elections');
    }
}

<?php

namespace App\Http\Controllers;

use App\Election;
use App\Party;
use Illuminate\Http\Request;

class PartiesController extends Controller
{
    public function index(Request $request){
    	if($request->input('all', false)){
			$parties = Party::inRandomOrder()->get();
			return response()->json($parties);
		}else{
    		$elections = $this->getByElection($request->input('year',date("Y")));
    		return response()->json($elections);
		}
	}

	public function show(Party $party){
    	return response()->json($party);
	}

	private function getByElection($year){
    	$elections = Election::where('year',$year)->orderBy('show_position')->get()->load(['ballots'=> function ($q) {
			$q->inRandomOrder();
		},'ballots.party']);

    	return $elections;
	}
}

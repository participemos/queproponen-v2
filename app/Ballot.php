<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ballot extends Model
{

	public function candidates(){
		return $this->hasMany(Candidate::class);
	}
	public function party(){
		return $this->belongsTo(Party::class);
	}


	public function election(){
		return $this->belongsTo(Election::class);
	}
}

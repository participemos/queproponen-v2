<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Election extends Model
{
    public function ballots(){
    	return $this->hasMany(Ballot::class);
	}
}
